# Guia básica de contenedores en Docker

Con está guia aprenderá sobre el funcionamiento y uso de los contenedores a través de Docker CLI, usando el principio de `Learn X concepts, not comands`. Sin embargo, no profundizaremos en los terminos básicos, como imagenes, volumenes y contenedores, así como tampoco en los pasos para la instalación. En la web, hay bastante información sobre esos terminos básicos.

Desde el inicio de la guia, se asume que ya está instalado Docker y comenzaremos a gestionar imagenes, luego contenedores y volúmenes y terminaremos aplicando los mismos pasos en un Dockerfile.

El proposito de la guia es armar un espacio de trabajo para migrar PHP 4 a PHP 5.6, en el que podremos probar un mismo sistema en dos entornos diferentes, y realizar las pruebas de la migración. Suponiendo que el host tiene instalado PHP 4, a traves de Docker haremos pruebas en PHP 5.6. Al final de la guia, tendremos dos contenedores corriendo, con diferentes propositos y caracteristicas.

- PHP 5.6
- PostgreSQL

También mencionaremos el despliegue de este espacio, para diferentes desarrolladores y así beneficiar al equipo con la versatilidad que ofrece Docker para la replicación de espacios de trabajo (Ejm. produccion, pruebas y desarrollo).

La version de Docker CLI, que se usará como referencia será:
```
$ docker version
Client:
	Version:           18.06.3-ce
	API version:       1.38
	Go version:        go1.10.3
	Git commit:        d7080c1
	Built:             Wed Feb 20 02:28:55 2019
	OS/Arch:           linux/amd64
	Experimental:      false
	
Server:
	Engine:
		Version:          18.06.3-ce
		API version:      1.38 (minimum version 1.12)
		Go version:       go1.10.3
		Git commit:       d7080c1
		Built:            Wed Feb 20 02:27:21 2019
		OS/Arch:          linux/amd64
		Experimental:     false
		
```
## Paso 1 - Seleccion de imagenes

El repositorio de Docker de donde se selecciona las imagenes es https://hub.docker.com/. No es necesario registarse para descargar las imagenes o para su busqueda, sin embargo Play With Docker https://labs.play-with-docker.com/ sí necesita una cuenta en Docker Hub. Con Play With Docker se puede probar todos los comando que existe para practicar, solo cuenta con 4 horas para hacer las pruebas, luego se borra el contenedor en Play With Docker.

Es necesario tener las imagenes que se van a utilizar en local, así que primero se deben descargar. El comando `docker pull`, consulta las imagenes en el repositorio Docker Hub, y descarga lo necesario para tener la imagen en local. El comando recibe un argumento, que es el nombre de la imagen y el tag, en el siguiente formato: `docker pull NAME[:TAG]`. Para nuestro caso, traeremos la imagen de PHP, en su version 5.6.9 con Apache y la imagen de PostgreSQL v10.9. Ambas imagenes son tomadas de las cuentas oficiales de PHP y PostgreSQL, en el repositorio.

`docker pull php:5.6.9-apache`

`docker pull postgres:10.9`

Cuando no se especifica el tag de la imagen en el comando, DockerCLI, asume que se está buscando la imagen con el tag mas actualizado, cuyo nombre por defecto es `latest`.

## Paso 2 - Levantar los contenedores

El comando `docker image ls`, muestra una la lista de imagenes que están disponibles en local y con las que podremos trabajar.

```
$ docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
postgres            10.9                897b33033d64        2 weeks ago         230MB
php                 5.6.9-apache        afdc6e9893d3        4 years ago         481MB
```

Ahora, queremos levantar el contenedor apartir de la imagen que tenemos. Primero, lo haremos con el contenedor para la base de datos.

Lo mas importante de los contenedores son los puertos y los volúmenes. Es importante que el contenedor tenga un _puerto expuesto_ y un _puerto publicado_. El puerto por defecto de PostgreSQL es el 5432, y en la imagen ese es el puerto que viene _expuesto_. El _puerto publicado_ se asigna al momento de crear el contenedor. Este _puerto publicado_ se usará para acceder al sistema dentro del contenedor. Usaremos el puerto 5200, en este ejemplo.

El volúmen en el contenedor debe enlazar una carpeta dentro del contenedor a una carpeta en local. De esa forma, lograremos mantener el código persistente en ambos entornos. Esta no es la única forma de manejar volúmenes ni datos persistentes, pero es la más práctica para nuestra guia.

Para levantar el contenedor usaremos el comando `docker run`, que crea un contenedor en caso de que no exista y ejecuta un comando dentro de ese contenedor.

```
$ docker run -d \
	--name postgres \
	--restart=always \
	-p 5200:5432 \
	-v /root/postgres_db:/var/lib/postgresql/data/pgdata \
	-e POSTGRES_PASSWORD=12345 -e PGDATA=/var/lib/postgresql/data/pgdata \
	postgres:10.9
```

Hay que distinguir que al crear el contenedor de PostgreSQL, se usan dos vecer la bandera `-e`. Este parametro hace referencia a "enviroment" y lo que hace es asignar valores a una variable de entorno para gestionar el motor de base de datos. Hay mas información sobre esas variables en la pag del repositorio de la imagen `https://hub.docker.com/_/postgres`. También conseguiran mas información sobre la imagen de PHP en la pag `https://hub.docker.com/_/php`.

El contenedor de PHP, usa una imagen que viene con Apache. Al igual que para PostgreSQL, se debe asignar puertos y volúmenes. El _puerto expuesto_ de Apache usualmente es el 80 y se quiere enlazar a un _puerto publicado_ diferente que el de PostgreSQL. Así que usaremos el 5201. De la misma forma, la carpeta mas importante de Apache es `/var/www/html`. Ahí es donde se colocaría el código. Con Docker, lo que se debe hacer es enlazar la carpeta en local donde está el código, con la carpeta dentro del contenedor. Suponiendo que el sistema se encuentra en `/root/php56`, usaremos esa carpeta para el enlace de los volúmenes.

```
$ docker run -d \
	--name php56 \
	--restart=always \
	-p 5201:80 \
	-v /root/php56:/var/www/html \
	php:5.6.9-apache
```

La primera bandera del comando `docker run` es `-d`, que crea el contenedor en background y muestra el id del contenedor creado. El parámetro `--name` es para asignar el nombre a un contenedor, debe ser único. Si no se asigna este parámetro se crea un nombre asignado por Docker. El parametro `--restart=always` inicia el contenedor siempre, así se reinicie el servidor se levanta automáticamente. La opción `-p` asigna el puerto al contenedor donde el primer número es el _puerto publicado_ que usa el usuario para acceder al contenedor y el segundo es el _puerto expuesto_ del contenedor. La opción `-v` es para el volumen, donde va estar el código del sistema. El volumen enlaza una carpeta en local a una carpeta dentro del contenedor. Y por ultimo se coloca el nombre de la imagen que se descargó, para el caso de PHP seria 'php:5.6.9-apache' esto busca primero la imagen si está descargada si en dado caso de no estar la imagen descargada se encarga de bajarla y crearía el contenedor.


Se puede ver los contenedores corriendo con el comando `docker container ls`.


```
$ docker container ls
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
4067736bdd71        php:5.6.9-apache    "apache2-foreground"     11 seconds ago      Up 9 seconds        0.0.0.0:5201->80/tcp     php56
919096ee677f        postgres:10.9       "docker-entrypoint.s…"   9 minutes ago       Up 9 minutes        0.0.0.0:5200->5432/tcp   postgres
```

## Paso 3 - Administración de contenedores

Ahora se quiere verificar que ambos contenedores funcionen correctamente. Para este ejemplo, agregaremos a la carpeta public/ un archivo `info.php`, para revisar las librerias y la version de PHP. Como se mencionó, el código del sistema se encuentra en la carpeta `/root/php56`, en local.

```
$ echo '<?php phpinfo(); ?>' > public/info.php
```

Recuerde que esté archivo lo creamos en local, dentro de la carpeta que está enlazada al contenedor.

Ahora se llama desde el navegador a la ruta dentro del contenedor, a traves del _puerto publicado_ de Apache, mediante la url direccion.servidor:5201/public/info.php (ejm: 127.0.0.1:5201/public/info.php)

Aquí se ve la información que se estaba buscando. Y se confirma que el contenedor de PHP, está funcionando correctamente, pero hace falta que el contenedor tenga la libreria para conectar a PostgreSQL. Esta libreria es la famosa PDO (pdo_pgsql), por lo que debemos instalarla en el contenedor. Y para esto es necesario ingresar a la consola del contenedor para ejecutar la instalacion junto con las respectivas dependencias de esa libreria.

Estando el contenedor corriendo, se hará un llamado al `bash` y ese comando se debe mantener activo e interactivo para poder realizar los cambios que se desean. 

`$ docker exec -ti php56 bash`

`docker exec`, ejecuta un comando dentro del contenedor, como puede ser `ls -l`, para ver la lista de los archivos de la carpeta en donde se encuentra. Las banderas `-ti`, permiten acceso tty y mantiene el bufer STDIN abierto. Luego de ejecutado este comando, veremos un prompt nuevo, parecido a este:
```
$ docker exec -ti php56 bash
root@f1090659c1e9:/var/www/html#
```
Ese prompt, indica que estamos dentro del contenedor y que cualquier cosa que se ejecute, afectará unicamente a ese contenedor. Como la imagen que se está usando es muy básica, se necesita instalarle un par de paquetes para que el sistema funcione correctamente. El modulo de PHP 5 para PostgreSQL es 'php5-pgsql', por lo que debemos actualizar los repositorios dentro del contenedor e instalar ese paquete:
```
# apt update
# apt install -y php5-pgsql libapache2-mod-php5
# exit

```

_Nota_: Durante la instalación de estos dos paquetes, se solicita cambiar el archivo php5.load, lo cual recomendamos hacerlo presionando 'Y' y luego enter.

Con el último comando `exit`, solo regresaremos al prompt local. Sin embargo, no se ha terminado de realizar todos los cambios, puesto que falta reiniciar el servicio de apache. Para esto, se mostrará otra forma de ejecutar comandos sin necesidad de ingresar en el contenedor. Lo que se necesita hacer es reinicar el deamon de apache2, `/etc/init.d/apache2 restart`, para que tome en cuenta las librerias que se acaban de instalar. 

`docker exec -ti php56 /etc/init.d/apache2 restart`

Como ya está listo el entorno PHP, se debe adecuar el de base de datos. Esto será muy similar a como se hizo anteriormente, solo que no es necesario incluir nuevas librerias/paquetes. Primero, hay que ingresar al contenedor creado, luego, en este caso, ingresar a la consola de PostgreSQL (psql) y ahí se procede a cargar la data. De forma simplificada, se hará así:

```
docker exec -ti postgres psql -U username -W dbname < dbexport.pgsql
```

Para finalizar, se debe conectar el sistema a la BD y como ambos se encuentran en un mismo host, corriendo en contenedores paralelos, se vuelve necesario conocer las ip que tienen y que puedan comunicarse. Las redes el docker se manejan como 'drivers' y el mas nos interesa es el que viene por defecto en docker: `bridge`. Sin embargo, solo revisaremos los datos que tiene y no entraremos al area de gestión de redes en docker, pues es un tema bastante extenso.

Por los momentos, buscaremos las ip asignadas en 'bridge' usando un comando:

`docker network inspect bridge`

Este comando muestra un archivo JSON con los datos y solo es necesario buscar la sección que nos interesa.

```
(...)
"Containers": {
            "ba7d39de164ea2bdbe2cd127449aaf0be3c49cd929f7ecf62418d56a103c4ad2": {
                "Name": "postgres",
                "EndpointID": "9ddda877c46e1e133a702fe4d94592453a12276c54c1540421777315cc83e296",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "bf8bb31139b53cc7cf86751b3e1af4e0a1ed958a960bd61817d560dc01a20b1a": {
                "Name": "php56",
                "EndpointID": "694340192d1497d5d4891db733500180aec91227f57e5cb4e33055b208b95b9c",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        },
(...)
```
Aquí se logra identificar la ip de el contenedor de PostgreSQL: `172.17.0.2`, lo cual es suficiente para conectar el sistema de nuestro ejemplo.


## Paso 4 - Replicar y compartir imagenes

Ahora está listo el contenedor de PHP 5.Y como es de notar, el contenedor es diferente al contenedor creado apartir de la imagen descargada. Lo recomendable aquí, es generar una nueva imagen, apartir de este contenedor para facilitar la replicación del entorno y la forma de compartirlo.

